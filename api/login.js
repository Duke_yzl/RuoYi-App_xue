import request from '@/utils/request'

// 登录方法
export function login(username, password, code, uuid, authId) {
  const data = {
    username,
    password,
    code,
    uuid,
	authId
  }
  return request({
    'url': '/login',
    headers: {
      isToken: false
    },
    'method': 'post',
    'data': data
  })
}

// 获取用户详细信息
export function getInfo() {
  return request({
    'url': '/getInfo',
    'method': 'get'
  })
}

// 退出方法
export function logout() {
  return request({
    'url': '/logout',
    'method': 'post'
  })
}

// 获取验证码
export function getCodeImg() {
  return request({
    'url': '/captchaImage',
    headers: {
      isToken: false
    },
    method: 'get',
    timeout: 20000
  })
}
// 第三方回调
export function getCallback(type,query) {
  return request({
    url: `/oauth/callback/${type}`,
    headers: {
      isToken: false
    },
    method: 'get',
    params: query
  })
}